# JS Debug Utils

This repo is a local dependency for the *frontend*. Its purpose is to strip out lingering `console.log`s and other calls that we don't want happening in prod

